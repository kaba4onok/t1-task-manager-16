package ru.t1.rleonov.tm.repository;

import ru.t1.rleonov.tm.api.repository.ICommandRepository;
import ru.t1.rleonov.tm.constant.TerminalArguments;
import ru.t1.rleonov.tm.constant.TerminalCommands;
import ru.t1.rleonov.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    private final static Command ABOUT = new Command(
            TerminalCommands.ABOUT, TerminalArguments.ABOUT,
            "Show application info."
    );

    private final static Command INFO = new Command(
            TerminalCommands.INFO, TerminalArguments.INFO,
            "Show system info."
    );

    private final static Command VERSION = new Command(
            TerminalCommands.VERSION, TerminalArguments.VERSION,
            "Show application version."
    );

    private final static Command EXIT = new Command(
            TerminalCommands.EXIT, null,
            "Exit application."
    );

    private final static Command HELP = new Command(
            TerminalCommands.HELP, TerminalArguments.HELP,
            "Show application commands."
    );

    private final static Command COMMANDS = new Command(
            TerminalCommands.COMMANDS, TerminalArguments.COMMANDS,
            "Show application commands."
    );

    private final static Command ARGUMENTS = new Command(
            TerminalCommands.ARGUMENTS, TerminalArguments.ARGUMENTS,
            "Show application commands."
    );

    private final static Command PROJECT_LIST = new Command(
            TerminalCommands.PROJECT_LIST, null,
            "Show project list."
    );

    private final static Command PROJECT_CREATE = new Command(
            TerminalCommands.PROJECT_CREATE, null,
            "Create new project."
    );

    private final static Command PROJECT_CLEAR = new Command(
            TerminalCommands.PROJECT_CLEAR, null,
            "Delete all projects."
    );

    private final static Command PROJECT_SHOW_BY_ID = new Command(
            TerminalCommands.PROJECT_SHOW_BY_ID, null,
            "Show project by id."
    );

    private final static Command PROJECT_SHOW_BY_INDEX = new Command(
            TerminalCommands.PROJECT_SHOW_BY_INDEX, null,
            "Show project by index."
    );

    private final static Command PROJECT_REMOVE_BY_ID = new Command(
            TerminalCommands.PROJECT_REMOVE_BY_ID, null,
            "Remove project by id."
    );

    private final static Command PROJECT_REMOVE_BY_INDEX = new Command(
            TerminalCommands.PROJECT_REMOVE_BY_INDEX, null,
            "Remove project by index."
    );

    private final static Command PROJECT_UPDATE_BY_ID = new Command(
            TerminalCommands.PROJECT_UPDATE_BY_ID, null,
            "Update project by id."
    );

    private final static Command PROJECT_UPDATE_BY_INDEX = new Command(
            TerminalCommands.PROJECT_UPDATE_BY_INDEX, null,
            "Update project by index."
    );

    private final static Command PROJECT_START_BY_ID = new Command(
            TerminalCommands.PROJECT_START_BY_ID, null,
            "Start project by id."
    );

    private final static Command PROJECT_START_BY_INDEX = new Command(
            TerminalCommands.PROJECT_START_BY_INDEX, null,
            "Start project by index."
    );

    private final static Command PROJECT_COMPLETE_BY_ID = new Command(
            TerminalCommands.PROJECT_COMPLETE_BY_ID, null,
            "Complete project by id."
    );

    private final static Command PROJECT_COMPLETE_BY_INDEX = new Command(
            TerminalCommands.PROJECT_COMPLETE_BY_INDEX, null,
            "Complete project by index."
    );

    private final static Command PROJECT_CHANGE_STATUS_BY_ID = new Command(
            TerminalCommands.PROJECT_CHANGE_STATUS_BY_ID, null,
            "Change project status by id."
    );

    private final static Command PROJECT_CHANGE_STATUS_BY_INDEX = new Command(
            TerminalCommands.PROJECT_CHANGE_STATUS_BY_INDEX, null,
            "Change project status by index."
    );

    private final static Command TASK_LIST = new Command(
            TerminalCommands.TASK_LIST, null,
            "Show task list."
    );

    private final static Command TASK_CREATE = new Command(
            TerminalCommands.TASK_CREATE, null,
            "Create new task."
    );

    private final static Command TASK_CLEAR = new Command(
            TerminalCommands.TASK_CLEAR, null,
            "Delete all tasks."
    );

    private final static Command TASK_SHOW_BY_ID = new Command(
            TerminalCommands.TASK_SHOW_BY_ID, null,
            "Show task by id."
    );

    private final static Command TASK_SHOW_BY_INDEX = new Command(
            TerminalCommands.TASK_SHOW_BY_INDEX, null,
            "Show task by index."
    );

    private final static Command TASK_REMOVE_BY_ID = new Command(
            TerminalCommands.TASK_REMOVE_BY_ID, null,
            "Remove task by id."
    );

    private final static Command TASK_REMOVE_BY_INDEX = new Command(
            TerminalCommands.TASK_REMOVE_BY_INDEX, null,
            "Remove task by index."
    );

    private final static Command TASK_UPDATE_BY_ID = new Command(
            TerminalCommands.TASK_UPDATE_BY_ID, null,
            "Update task by id."
    );

    private final static Command TASK_UPDATE_BY_INDEX = new Command(
            TerminalCommands.TASK_UPDATE_BY_INDEX, null,
            "Update task by index."
    );

    private final static Command TASK_START_BY_ID = new Command(
            TerminalCommands.TASK_START_BY_ID, null,
            "Start task by id."
    );

    private final static Command TASK_START_BY_INDEX = new Command(
            TerminalCommands.TASK_START_BY_INDEX, null,
            "Start task by index."
    );

    private final static Command TASK_COMPLETE_BY_ID = new Command(
            TerminalCommands.TASK_COMPLETE_BY_ID, null,
            "Complete task by id."
    );

    private final static Command TASK_COMPLETE_BY_INDEX = new Command(
            TerminalCommands.TASK_COMPLETE_BY_INDEX, null,
            "Complete task by index."
    );

    private final static Command TASK_CHANGE_STATUS_BY_ID = new Command(
            TerminalCommands.TASK_CHANGE_STATUS_BY_ID, null,
            "Change task status by id."
    );

    private final static Command TASK_CHANGE_STATUS_BY_INDEX = new Command(
            TerminalCommands.TASK_CHANGE_STATUS_BY_INDEX, null,
            "Change task status by index."
    );

    private final static Command TASK_BIND_TO_PROJECT = new Command(
            TerminalCommands.TASK_BIND_TO_PROJECT, null,
            "Bind task to project."
    );

    private final static Command TASK_UNBIND_FROM_PROJECT = new Command(
            TerminalCommands.TASK_UNBIND_FROM_PROJECT, null,
            "Unbind task from project."
    );

    private final static Command TASK_SHOW_BY_PROJECT_ID = new Command(
            TerminalCommands.TASK_SHOW_BY_PROJECT_ID, null,
            "Show task list by project id."
    );

    private final static Command[] TERMINAL_COMMANDS = new Command[]{
            COMMANDS, ARGUMENTS, ABOUT, INFO, VERSION, HELP,

            PROJECT_LIST, PROJECT_CREATE, PROJECT_CLEAR,
            PROJECT_SHOW_BY_ID, PROJECT_SHOW_BY_INDEX,
            PROJECT_REMOVE_BY_ID, PROJECT_REMOVE_BY_INDEX,
            PROJECT_UPDATE_BY_ID, PROJECT_UPDATE_BY_INDEX,
            PROJECT_START_BY_ID, PROJECT_START_BY_INDEX,
            PROJECT_COMPLETE_BY_ID, PROJECT_COMPLETE_BY_INDEX,
            PROJECT_CHANGE_STATUS_BY_ID, PROJECT_CHANGE_STATUS_BY_INDEX,

            TASK_LIST, TASK_CREATE, TASK_CLEAR,
            TASK_SHOW_BY_ID, TASK_SHOW_BY_INDEX, TASK_SHOW_BY_PROJECT_ID,
            TASK_REMOVE_BY_ID, TASK_REMOVE_BY_INDEX,
            TASK_UPDATE_BY_ID, TASK_UPDATE_BY_INDEX,
            TASK_START_BY_ID, TASK_START_BY_INDEX,
            TASK_COMPLETE_BY_ID, TASK_COMPLETE_BY_INDEX,
            TASK_CHANGE_STATUS_BY_ID, TASK_CHANGE_STATUS_BY_INDEX,
            TASK_BIND_TO_PROJECT, TASK_UNBIND_FROM_PROJECT,

            EXIT
    };

    @Override
    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
