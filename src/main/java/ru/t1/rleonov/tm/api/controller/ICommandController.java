package ru.t1.rleonov.tm.api.controller;

public interface ICommandController {

    void showAbout();

    void showVersion();

    void showInfo();

    void showHelp();

    void showArguments();

    void showCommands();

}
