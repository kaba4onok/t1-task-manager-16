package ru.t1.rleonov.tm.api.repository;

import ru.t1.rleonov.tm.model.Project;
import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    Integer getSize();

    Project add(Project project);

    List<Project> findAll();

    List<Project> findAll(Comparator comparator);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project remove(Project project);

    Project removeById (String id);

    Project removeByIndex (Integer index);

    void clear();

}
