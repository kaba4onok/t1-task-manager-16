package ru.t1.rleonov.tm.api.service;

import ru.t1.rleonov.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
