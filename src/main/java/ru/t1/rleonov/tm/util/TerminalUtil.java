package ru.t1.rleonov.tm.util;

import ru.t1.rleonov.tm.exception.field.IndexIncorrectException;
import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextNumber() {
        final String value = nextLine();
        try {
            return Integer.parseInt(value);
        } catch (final Exception e) {
            throw new IndexIncorrectException();
        }
    }

}
